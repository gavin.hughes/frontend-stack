var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var path = require('path');
var notify        = require('gulp-notify');
var compass = require('gulp-compass');
var shell = require('gulp-shell');
var csslint = require('gulp-csslint');

var autoprefixer = require('gulp-autoprefixer');

var config = require('./config.json');

gulp.task('serve', function() {
    // Allow cross origin usuefull for android winre debugging
    browserSync.init({
        proxy: config.project.url,
        middleware: function (req, res, next) {
          res.setHeader('Access-Control-Allow-Origin', '*');
          next();
        }
    });

    gulp.watch(config.project.sassDir,  {debounceDelay: 2000}, ['styles']);
    gulp.watch([config.project.cssDir], {debounceDelay: 2000}).on('change', browserSync.reload);
    gulp.watch(config.project.templates).on('change', function(){
        gulp.run('drush', browserSync.reload);
    });
});


gulp.task('default', ['serve']);


gulp.task('styles',function(){
        return gulp.src(config.project.sassDir)
        .pipe(compass({
          project: path.join(__dirname, config.project.themePath),
          config_file: 'config.rb',
          comments: true,
          bundle_exec:true}))
        .pipe(autoprefixer(config.autoprefixer))
        .pipe(gulp.dest('css/'));
});

gulp.task('prefix',function(){
        return gulp.src(config.project.cssDir)
        .pipe(autoprefixer(config.autoprefixer))
        .pipe(gulp.dest(config.project.themePath + '/css/'));
});

gulp.task('drush', shell.task([
  'drush cache-clear theme-registry'
]));

gulp.task('cc', shell.task([
  'git checkout ' + config.project.themePath + 'css/'
]));

gulp.task('genc', shell.task([
  'git commit '  + config.project.cssDir + ' -m "gen css"'
]));

gulp.task('co', shell.task([
  'git checkout ' + config.project.cssDir
]));


